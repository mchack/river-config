#!/bin/sh

# Super+Shift+Return to start an instance of foot (https://codeberg.org/dnkl/foot)
riverctl map normal Super+Shift Return spawn foot

# Super+Q to close the focused view
riverctl map normal Super Q close

# Super+Shift+E to exit river
riverctl map normal Super+Shift E exit

# Super+J and Super+K to focus the next/previous view in the layout stack
riverctl map normal Super J focus-view next
riverctl map normal Super K focus-view previous

# Super+Shift+J and Super+Shift+K to swap the focused view with the next/previous
# view in the layout stack
riverctl map normal Super+Shift J swap next
riverctl map normal Super+Shift K swap previous

# Get back to last tag
riverctl map normal Super Tab focus-previous-tags

# Super+Period and Super+Comma to focus the next/previous output
riverctl map normal Super Period focus-output next
riverctl map normal Super Comma focus-output previous

# Super+Return to bump the focused view to the top of the layout stack
riverctl map normal Super Return zoom

riverctl map normal Super P spawn 'bemenu-run'

## mctile

# Super+H and Super+L to decrease/increase the main ratio
riverctl map normal Super H send-layout-cmd tile "main_ratio -0.05"
riverctl map normal Super L send-layout-cmd tile "main_ratio +0.05"

# Increase/decrease number of views in main track
riverctl map normal Super I send-layout-cmd tile "main_count +1"
riverctl map normal Super D send-layout-cmd tile "main_count -1"

# Change to monocle mode
riverctl map normal Super M send-layout-cmd tile "monocle"

# Change to tiling
riverctl map normal Super T send-layout-cmd tile "tile"

# Start programs
riverctl map normal Super P spawn "bemenu-run"

# Super+Alt+{H,J,K,L} to move views
riverctl map normal Super+Alt H move left 100
riverctl map normal Super+Alt J move down 100
riverctl map normal Super+Alt K move up 100
riverctl map normal Super+Alt L move right 100

# Super+Alt+Control+{H,J,K,L} to snap views to screen edges
riverctl map normal Super+Alt+Control H snap left
riverctl map normal Super+Alt+Control J snap down
riverctl map normal Super+Alt+Control K snap up
riverctl map normal Super+Alt+Control L snap right

# Super+Alt+Shif+{H,J,K,L} to resize views
riverctl map normal Super+Alt+Shift H resize horizontal -100
riverctl map normal Super+Alt+Shift J resize vertical 100
riverctl map normal Super+Alt+Shift K resize vertical -100
riverctl map normal Super+Alt+Shift L resize horizontal 100

# Super + Left Mouse Button to move views
riverctl map-pointer normal Super BTN_LEFT move-view

# Super + Right Mouse Button to resize views
riverctl map-pointer normal Super BTN_RIGHT resize-view

for i in $(seq 1 9)
do
    tags=$((1 << ($i - 1)))

    # Super+[1-9] to focus tag [0-8]
    riverctl map normal Super $i set-focused-tags $tags

    # Super+Shift+[1-9] to tag focused view with tag [0-8]
    riverctl map normal Super+Shift $i set-view-tags $tags

    # Super+Ctrl+[1-9] to toggle focus of tag [0-8]
    riverctl map normal Super+Control $i toggle-focused-tags $tags

    # Super+Shift+Ctrl+[1-9] to toggle tag [0-8] of focused view
    riverctl map normal Super+Shift+Control $i toggle-view-tags $tags
done

# Super+0 to focus all tags
# Super+Shift+0 to tag focused view with all tags
all_tags=$(((1 << 32) - 1))
riverctl map normal Super 0 set-focused-tags $all_tags
riverctl map normal Super+Shift 0 set-view-tags $all_tags

# Super+Space to toggle float
riverctl map normal Super Space toggle-float

# Super+F to toggle fullscreen
riverctl map normal Super F toggle-fullscreen

# Declare a passthrough mode. This mode has only a single mapping to return to
# normal mode. This makes it useful for testing a nested wayland compositor
riverctl declare-mode passthrough

# Super+F11 to enter passthrough mode
riverctl map normal Super F11 enter-mode passthrough

# Super+F11 to return to normal mode
riverctl map passthrough Super F11 enter-mode normal

# Take screenshot of whole screen
riverctl map normal Super S spawn 'grim ~/screenshots/$(date +%Y-%m-%d_%H-%M-%s).png'

# Take screenshot of area
riverctl map normal Super+Shift S spawn 'grim -g "$(slurp)" ~/screenshots/$(date +%Y-%m-%d_%H-%M-%s).png'

# Various media key mapping examples for both normal and locked mode which do
# not have a modifier
for mode in normal locked
do
    # Control audio volume
    riverctl map $mode None XF86AudioRaiseVolume  spawn 'amixer -q sset Master 1%+'
    riverctl map $mode None XF86AudioLowerVolume  spawn 'amixer -q sset Master 1%-'
    riverctl map $mode None XF86AudioMute         spawn 'amixer set Master 1+ toggle'

    # Control screen backlight brighness
    riverctl map $mode None XF86MonBrightnessUp   spawn 'brightnessctl s 5%+'
    riverctl map $mode None XF86MonBrightnessDown spawn 'brightnessctl s 5%-'
done

# Set background and border color
riverctl background-color 0x222222
riverctl border-color-focused 0xd80000
riverctl border-color-unfocused 0x586e75
riverctl border-color-urgent 0xf86a59
riverctl border-width 1

# Attach new windows at the bottom of the stack
riverctl attach-mode bottom

# Set keyboard repeat rate
riverctl set-repeat 40 240

# Internal trackpoint
riverctl input pointer-2-10-TPPS/2_Elan_TrackPoint pointer-accel -0.25
riverctl input pointer-2-10-TPPS/2_Elan_TrackPoint accel-profile adaptive

# External trackpoint
riverctl input keyboard-6127-24647-Lenovo_ThinkPad_Compact_USB_Keyboard_with_TrackPoint pointer-accel -0.30
riverctl input keyboard-6127-24647-Lenovo_ThinkPad_Compact_USB_Keyboard_with_TrackPoint accel-profile adaptive

# Turn off the Thinkpad touchpad
riverctl input pointer-1739-52824-SYNA8008:00_06CB:CE58_Touchpad events disabled

# Hide the cursor
riverctl hide-cursor timeout 4000
riverctl hide-cursor when-typing enabled

# Make certain views start floating
riverctl float-filter-add app-id float
riverctl float-filter-add title "popup title with spaces"

# Focus follows mouse
riverctl focus-follows-cursor normal

# Let the cursor follow focused screen
riverctl set-cursor-warp on-output-change

# Shift whitebalance to red in the evenings
wlsunset -l 55.6 -L 13.1 -t 3000 -T 5000 &

# Notifications
mako --background-color \#000000 --text-color \#36ff00 --border-color \#36ff00 &

# Dismiss any lingering notifications with Super+Escape
riverctl map normal Super Escape spawn "makoctl dismiss"

# For screen sharing
dbus-update-activation-environment --systemd WAYLAND_DISPLAY

# Show tags when we change tag
river-tag-overlay&

# Status bar
i3bar-river&

# Dynamic monitor configuration
kanshi &

# Lock screen when idle
swayidle &

# Use my own tiling engine
riverctl default-layout tile
exec mctile

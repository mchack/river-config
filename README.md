# MC's River configuration

This is [my](https://hack.org/mc) configuration for the [River Wayland
compositor](https://github.com/riverwm/river) to be used with [my
layout generator](https://codeberg.org/mchack/mctile).
